/**
 * Created by faviofigueroa on 29/7/16.
 */
(function ($) {


    var sucursal = localStorage.getItem("sucursal_panel");
    var user = localStorage.getItem("userPXP_panel");
    var contra = localStorage.getItem("contraPXP_panel");

    config.usPxp = user;
    config.pwdPxp = contra;
    config.tipo_ruta = localStorage.getItem("tipoHost_panel");

    if (localStorage.getItem("tipoHost_panel") == "DOMINIO") {
        var dominio = localStorage.getItem("dominioPXP_panel");

        config.DOMINIO.url = dominio;

    } else if (localStorage.getItem("tipoHost_panel") == "IP") {

        var host = localStorage.getItem("hostPXP_panel");
        var carpeta = localStorage.getItem("carpetaPxp_panel");

        config.IP.ip = host;
        config.IP.carpeta = carpeta;

    } else {
        alert("hay un error en su configuracion revisa tu configuracion");
    }


    config.init();

    /*
    *
    * localStorage.setItem("tiempoEspera_panel", $("#tiempo_espera_peticion").val());
     localStorage.setItem("tiempoNuevo_panel", $("#tiempo_nueva_peticion_panel").val());
     localStorage.setItem("sonidoLLamadoTono", $("#sonido_llamado_tono").val());
     localStorage.setItem("volumenLLamadoTono", $("#volumen_sonido_llamado_tono").val());
     localStorage.setItem("sonidoLLamadoVoz", $("#sonido_llamado_voz").val());
     localStorage.setItem("volumenLLamadoVoz", $("#volumen_sonido_llamado_voz").val());
     */


    panel = {
        tiempoEspera_panel:localStorage.getItem("tiempoEspera_panel"),
        tiempoNuevo_panel:localStorage.getItem("tiempoNuevo_panel"),
        sonidoLLamadoTono:localStorage.getItem("sonidoLLamadoTono"),
        volumenLLamadoTono:localStorage.getItem("volumenLLamadoTono"),
        sonidoLLamadoVoz:localStorage.getItem("sonidoLLamadoVoz"),
        volumenLLamadoVoz:localStorage.getItem("volumenLLamadoVoz"),
        volumenVideo:localStorage.getItem("volumenVideo"),

        primera_vez: 'si',
        fecha_ultima_llamada: "",
        alert: 'ekiga-vm.wav',
        cola: [],
        cola_vista: [],
        enProceso: false,
        limit: 6,
        dir: 'desc',
        hacerAjax: 'si',
        countError:0,
        init: function () {
            console.log(this)

            if (panel.sonidoLLamadoTono == 'si'){
                panel.Alert.play();
            }


            /*var ficha= {
             sigla:"abc-150"
             };
             this.Speech.play(ficha);*/
            this.panelAjax();


        },
        Alert: {

            play: function (filename) {
                filename = filename || panel.alert;
                $("#alert").attr("src", 'media/alert/' + filename);
                $("#alert")[0].play();
                $("#alert")[0].volume = localStorage.getItem("volumenLLamadoTono") / 10;

            }
        },
        panelAjax: function () {
            var self = this;
            ajax_dyd.data = {
                start: "0",
                limit: self.limit,
                sort: "ultima_llamada",
                dir: this.dir,
                id_sucursal: sucursal,
                fecha_pantalla: self.fecha_ultima_llamada
            };
            ajax_dyd.type = 'POST';
            ajax_dyd.url = 'pxp/lib/rest/colas/Ficha/llamarFichaPantalla';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = true;
            ajax_dyd.timeout_= panel.tiempoEspera_panel * 1000;
            ajax_dyd.peticion_ajax(function (resp) {


                self.hacerAjax = 'si';
                //vemos si existe un error en el servidor
                if(resp.ROOT != undefined){

                    alert("Ha ocurrido un error en el servidor. Revise los logs y actualice la pagina");
                    self.hacerAjax = 'no';
                    return;

                }

                if (resp.estado == "error") {
                    
                    self.hacerAjax = 'no';
                    self.countError++;
                    if(self.countError < 3){
                        console.log('error pero intentara nuevamente');
                        setTimeout(function () {
                            self.panelAjax();
                        }, panel.tiempoNuevo_panel*1000);
                    }else{
                        console.log("error dejara de hacer peticiones");
                        if(resp.t==="timeout") {
                            alert("Problemas de red , esta demorando demasiado la peticion, actualizar por favor");
                        } else {
                            alert(t+" actualizar por favor");
                        }
                    }
                    return;
                }



                if (self.hacerAjax == 'si') {

                    self.countError = 0;

                    if (resp.datos.length > 0) {
                        self.fecha_ultima_llamada = resp.datos[0].fecha_respuesta;
                    }

                    //cuando inicia recien la pagina
                    if (self.primera_vez == 'si') {

                        self.primera_vez = 'no';
                        $.each(resp.datos, function (k, v) {

                            self.cola_vista.push(v);

                            //va a dibujar los 6 objetos no pueden ser mas
                            $(".contenedor_cajas").append('<div id="' + v.sigla + '" class="caja_panel">' + v.sigla + ' -> ' + v.letra_ventanila + v.numero_ventanilla + '</div>');


                        });
                        self.limit = 1000;
                        self.dir = "asc";


                        console.log('cola vista', self.cola_vista)

                    } else if (self.primera_vez = 'no') {//cuando ya es la segunda peticion

                        if (resp.datos.length > 0) {


                            $.each(resp.datos, function (k, v) {
                                self.cola.push(v);

                            });


                            if (self.enProceso == false) {
                                console.log('ya no hay cola se inicia otra vez el proceso');
                                self.procesarCola();
                            }


                        }

                    }


                    setTimeout(function () {
                        self.panelAjax();
                    }, panel.tiempoNuevo_panel*1000);
                }


            });


        },
        procesarCola: function () {

            if (this.cola.length > 0) {

                this.enProceso = true;

                panel.Alert.play();


                var atendimiento = this.cola.shift(); // agrego el atendimiento nuevo segun su entrada

                var result = $.grep(this.cola_vista, function (e) {
                    return e.sigla == atendimiento.sigla;
                });

                console.log('result', result);

                var rellamada = 'no';
                //vemos si existe ya un llamado si existe es una re llamada
                if (result.length > 0) {

                    console.log('entra a re llamada')
                    var data = $.grep(this.cola_vista, function (e) {
                        return e.sigla != atendimiento.sigla;
                    });

                    //aca inicio la cola vista con todos los datos iguales pero quitanto la re llamada
                    this.cola_vista = data;

                    var quitar_atendimiento = {
                        sigla: atendimiento.sigla
                    };//quito el objeto de los datos el mas antiguo

                    rellamada = 'si';

                }


                this.cola_vista.push(atendimiento);//agrego a los datos de la vista el nuevo atendimiento


                if (rellamada == 'no') {
                    if (this.cola_vista.length <= 6) {
                        console.log('menor a 6')
                    } else {

                        this.cola_vista.shift();
                        console.log('igual o mayor a 6')

                    }

                }

                /*if(this.cola_vista.length > 5 && rellamada == 'no' ){
                 console.log('entra a remover')
                 console.log('quitar',quitar_atendimiento)
                 $("#"+quitar_atendimiento.sigla).remove(); //elimino de la vista html

                 }*/


                $(".contenedor_cajas").empty();

                $.each(this.cola_vista, function (k, v) {

                    $(".contenedor_cajas").prepend('<div id="' + v.sigla + '" class="caja_panel">' + v.sigla + ' -> ' + v.letra_ventanila + v.numero_ventanilla + '</div>');


                });


                //cambiamos el color de la letra
                $("#" + atendimiento.sigla).addClass("llamado");
                setTimeout(function () {
                    //volvemos a poner el color normal despues de llamar
                    $("#" + atendimiento.sigla).removeClass("llamado");
                }, 1800);

                //dibujo a la vista html el atendimientos
                //$(".contenedor_cajas").prepend('<div id="'+atendimiento.sigla+'" class="caja_panel">'+atendimiento.sigla+' -> '+atendimiento.letra_ventanila+atendimiento.numero_ventanilla+'</div>');

                $("#" + atendimiento.sigla).fadeTo(300, 0.1).fadeTo(300, 1.0).fadeTo(300, 0.1).fadeTo(300, 1.0).fadeTo(300, 0.1).fadeTo(300, 1.0);


                if(panel.sonidoLLamadoVoz == "si"){
                    this.Speech.play(atendimiento);
                }



            } else {
                this.enProceso = false;
                return;
            }


        },
        Speech: {
            queue: [],


            play: function (ficha) {

                //ficha audio
                this.queue.push({name: "ficha"});

                var sigla_num = ficha.sigla.split('-');

                //hacemos que hable la sigla las letras
                var sigla = sigla_num[0];
                sigla = sigla.toLowerCase();
                for (var i = 0; i < sigla.length; i++) {
                    this.queue.push({name: sigla.charAt(i).toLowerCase()});
                }


                //haacemos que hable los numeros
                var num = parseInt(sigla_num[1]);
                num = num + "";
                if (num.length == 3) {

                    var res = num.charAt(0);
                    console.log(res)
                    var ciento = "";
                    switch (parseInt(res)) {
                        case 1:
                            ciento = "ciento";
                            break;
                        case 2:
                            ciento = "docientos";
                            break;
                        case 3:
                            ciento = "trescientos";
                            break;
                        case 4:
                            ciento = "cuatrocientos";
                            break;
                        case 5:
                            ciento = "quiñientos";
                            break;
                        case 6:
                            ciento = "seiscientos";
                            break;
                        case 7:
                            ciento = "sietecientos";
                            break;
                        case 8:
                            ciento = "ochocientos";
                            break;
                        case 9:
                            ciento = "novecientos";
                            break;
                    }
                    this.queue.push({name: ciento});

                    var res2 = num.charAt(1) + num.charAt(2);
                    var res2 = parseInt(res2);
                    res2 = res2 + "";
                    this.queue.push({name: res2});
                    console.log('asd', this.queue)

                } else if (num.length == 2) {

                    var res2 = num.charAt(0) + num.charAt(1);
                    this.queue.push({name: res2});

                } else if (num.length == 1) {
                    this.queue.push({name: num});
                }

                //tipo de ventanilla
                this.queue.push({name: ficha.desc_tipo_ventanilla.toLowerCase()});

                //numero de ventanilla
                this.queue.push({name: ficha.numero_ventanilla});

                this.processQueue();

            },


            playFile: function (filename) {
                var self = this;
                console.log('llega audio')
                var bz = new buzz.sound(filename, {
                    formats: ["wav"],
                    autoplay: true,
                    volume:localStorage.getItem("volumenLLamadoVoz")*10
                });

                bz.bind("ended", function () {
                    buzz.sounds = [];
                    console.log('llega audio')
                    self.processQueue();


                });
            },

            processQueue: function () {
                var self = this;
                if (this.queue.length === 0) {
                    panel.procesarCola();
                    return;
                }
                if (buzz.sounds.length > 0) {
                    return;
                }
                var current = this.queue.shift();
                var filename = "media/audios/" + current.name; // toque

                this.playFile(filename);
                //verificamos si existe
                /*if(existeUrl(filename)){
                    this.playFile(filename);
                }else{ //si no existe volvemos aprocesar directamente el siguiente

                    this.processQueue();
                }*/

                 function existeUrl(url) {
                    var http = new XMLHttpRequest();
                    http.open('HEAD', url+".wav", false);
                    http.send();
                    return http.status!=404;
                }


            }
        },
        media: [],
        nombreActualReproducido: "",
        MediaVideosImagenes: function () {

            var self = this;
            ajax_dyd.data = {start: "0", limit: "10", sort: "id_ficha", dir: "ASC", tipo: "videos"};
            ajax_dyd.type = 'POST';
            ajax_dyd.url = 'pxp/lib/rest/colas/Parametrizacion/verArchivos';
            ajax_dyd.dataType = 'json';
            ajax_dyd.async = true;
            ajax_dyd.peticion_ajax(function (callback) {

                self.media = callback.datos;
                self.cargarVideos();
            });

        },
        verVideos:function () {

            $.ajax({
                url: 'videos.json',
                dataType: 'json',
                type: 'POST',
                //contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jQxhr) {
                    console.log('resp',data);



                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXhr);
                    console.log(textStatus);
                }
            });
        },
        cargarVideos: function () {


            var self = this;

            self.verVideos();

            var reproductor = $("#reproductor"),
                videos = this.media;
            console.log(this.media)

            //info = document.getElementById("info2");

            //info.innerHTML = "Vídeo: " + videos[0];
            console.log($("#reproductor"));
            $("#reproductor").attr("src", "media/videos/" + videos[0]);
            $("#reproductor").get(0).play();
            $("#reproductor").get(0).volume = panel.volumenVideo/10;

            self.nombreActualReproducido = videos[0];

            $('#reproductor').on('ended', function () {


                var nombreActual = self.nombreActualReproducido;
                var actual = videos.indexOf(nombreActual);
                console.log(actual);
                actual = actual == videos.length - 1 ? 0 : actual + 1;
                this.src = "media/videos/" + videos[actual];
                this.type = "video/mp4";
                self.nombreActualReproducido = videos[actual];


                this.play();

            });

            $('#reproductor').on('error', function (e) {

                console.log(e)
                console.log(this)
                alert("ocurrio un error con el video");
            });


            //reproductor.muted = true;


            /* reproductor.volume = 0.05;

             console.log(videos[0])*/

            /*reproductor.addEventListener("ended", function() {
             var nombreActual = info.innerHTML.split(": ")[1];
             var actual = videos.indexOf(nombreActual);
             console.log(actual);
             actual = actual == videos.length - 1 ? 0 : actual + 1;
             this.src = "includes/videos/"+videos[actual] + ".mp4";
             this.type="video/mp4";
             info.innerHTML = "Vídeo: " + videos[actual];

             if(videos[actual] == "adulto_mayor"){
             reproductor.volume = 0.05;
             }else{
             reproductor.volume = 0;
             }
             this.play();


             }, false);*/


        },
        errorVideo : function (e) {

            console.log('ERRORRRRR',e)
        }




    }

    panel.init();
    panel.MediaVideosImagenes();


})(jQuery);